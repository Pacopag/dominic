from django.forms.models import model_to_dict
from django.views.generic import View
from rest_framework.response import Response
from rest_framework.generics import ListCreateAPIView, GenericAPIView
from teams.models import Team, Player

class TeamsView(ListCreateAPIView):
	pass

class TopThreeView(GenericAPIView):
	def get(self, request, *args, **kwargs):
		return Response()
