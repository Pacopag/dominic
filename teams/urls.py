from django.conf.urls import url, patterns
from teams.views import *


urlpatterns = patterns('',
    url(r'^top-three', TopThreeView.as_view()),
    url(r'^$', TeamsView.as_view()),
)