from django.db import models
from django.utils import timezone

class Team(models.Model):
	name = models.CharField(max_length=255)
	city = models.CharField(max_length=255)
	created = models.DateTimeField(default=timezone.now)

	def get_players(self):
		return Player.objects.filter(team=self)

class Player(models.Model):
	name = models.CharField(max_length=255)
	number = models.IntegerField()
	points = models.IntegerField(default=0)
	team = models.ForeignKey(Team)
