from django.contrib import admin
from teams.models import Team, Player

admin.site.register(Team)
admin.site.register(Player)
